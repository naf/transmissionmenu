# transmission dmenu frontend
this is a simple frontend for transmission daemon that uses dmenu.

## requirements

- [transmission daemon](https://transmissionbt.com/)
- [dmenu](https://tools.suckless.org/dmenu/)

## features

- add torrent: allows adding a torrent file to transmission.
- remove torrent: provides a list of torrents to select and remove from transmission.
- list torrents: displays a list of active torrents along with their details.
- quit: exits the frontend.
