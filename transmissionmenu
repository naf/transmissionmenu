#!/usr/local/bin/bash
# fix sequence files
# set -x
# set -e

startpaused=0
#dl="$(transmission-remote -si | sed -n 's/^.*Download directory: *//p')"

startdaemon () {
	transmission-daemon -e ~/.log/transmission
}

sequence () {
	transmission-remote -t$torrentid -pl all
	until [ "$(transmission-remote -t$torrentid -i  | sed -n 's/.*Percent Done: *//p')" == "100.0%" ]; do
		[ -z "$(transmission-remote -t$torrent_id -f)" ] && break
		file1=$(transmission-remote -t$torrent_id -f | sed '1,2d' | sed '/100%/d' | head -1 | awk '{print $1}'| sed "s/:.*//g" )
		file2=$(transmission-remote -t$torrent_id -f | sed '1,2d' | sed '/100%/d' | head -2 | awk '{print $1}'| tail -1 | sed "s/:.*//g" )
		file3=$(transmission-remote -t$torrent_id -f | sed '1,2d' | sed '/100%/d' | head -3 | awk '{print $1}'| tail -1 | sed "s/:.*//g" )
		[ -z "$file1" ] || transmission-remote -t$torrent_id -ph "$file1"
		[ -z "$file2" ] || transmission-remote -t$torrent_id -pn "$file2"
		[ -z "$file3" ] || transmission-remote -t$torrent_id -pn "$file3"
		until [[ "$(transmission-remote -t$torrent_id -f | grep "$file1" | head -1 | awk '{print $2}')" == *"100"* ]]; do
			sleep 5
		done
	done
}

navigate () {

	local current="$1"
	local list="$(find "$current" -maxdepth 1 -mindepth 1 -type d ! -name '.*' | sed 's/.*\///g')"
	local selected=$(printf ".\n..\n${list}" | dmenu -l 10 -p "PATH: $current")
	
	if [ -z "$selected" ]; then 
		return
	elif [ "$selected" == "." ]; then
		echo "$current"
	elif [ "$selected" == ".." ]; then
		navigate "$(dirname $current)"
	elif [ -d "$current/$selected" ]; then
		navigate "$current/$selected"
	else
		return
	fi
}

setlocation () {
	location=$(navigate "$HOME")
	if [ -d "$location" ]; then
		action=$(printf "move\nfind\n"| dmenu -p "TORRENT'S DATA: ")
		transmission-remote -t$id --$action $location
	else
		return
	fi
}

speedlimits () {
	local down=$(dmenu -p "download speed limit in kB/s or [u]nlimited (current: $downspeed) :" <&-)
	if [ ! -z $down ]; then
		[ "$down" == "u" ] && transmission-remote -D || transmission-remote -d "$down"
	fi
	local up=$(dmenu -p "upload speed limit ($upspeed) [unlimited]:" <&-)
	if [ ! -z $up ]; then
		[ "$up" == "u" ] && transmission-remote -U || transmission-remote -u "$up"
	fi
}

listtorrents () {
	while read line; do
		local id="$(echo $line | awk '{printf $1}'| sed 's/*//g')"
		local name=$(transmission-remote -t$id -i | sed -n 's/^.*Name: *//p')
		local percent=$(transmission-remote -t$id -i | sed -n 's/^.*Percent Done: *//p')
		local size="$(transmission-remote -t$id -i | sed -n 's/.*Total size: *\(.*\) (.*/\1/p')"
		local state=$(transmission-remote -t$id -i | sed -n 's/^.*State: *\(....\).*/\1/p')
		local err=$(transmission-remote -t$id -i | sed -n 's/  Error: //p' | cut -c -14)
		local ratio=$(transmission-remote -t$id -i | sed -n 's/.*Ratio: *//p')
		if [ ! -z "$err" ];then
			echo "$id|error|$size|$percent|$ratio|$(echo "$name" | cut -c -70) ($err)"
		elif [ ! -z "$state" ];then
			echo "$id|$state|$size|$percent|$ratio|$name"
		fi
	done < <(transmission-remote -l | awk '/^[[:space:]]*[0-9]/ {printf "%03d\n", $1}') | column -t -s "|" | dmenu -i -l 30
}

listtorrentfiles () {
	while read line; do
		local id=$(echo "$line" | sed 's/:.*//g')
		local name=$(echo "$line" | cut -d'/' -f2-)
		local percent=$(echo "$line" | awk '{print $2}')
		local get=$(echo "$line" | awk '{print $4}')
		local priority=$(echo "$line" | awk '{print $3}')
		local size=$(echo "$line" | awk '{print $5" "$6}')
		echo "$id|$priority|$get|$size|$percent|$name"
	done < <(transmission-remote -t$torrentid -f) | sed '1,2d' | column -t -s "|" | dmenu -i -l 30
}

managefiles () {

	fileline=$(listtorrentfiles) || return
	fileid=$(echo "$fileline" | awk '{print $1}')
	filename="$(echo "$fileline" | sed 's/.*| //g')"
	fileget="$(echo "$fileline" | cut -d'|' -f3)"

	if [[ "$fileget" == *"Yes"* ]]; then
		action=$(printf "open\npriority\nunmark" | dmenu -i -p "$(printf "$torrentname"| cut -c -90):") || return
	elif [[ "$file_get" == *"No"* ]]; then
		action=$(printf "open\npriority\nmark" | dmenu -i -p "$(printf "$torrentname"| cut -c -90):") || return
	else
		action=$(printf "open\npriority" | dmenu -i -p "$(printf "$torrentname"| cut -c -90):") || return
	fi

	case "$action" in
		"open") xdg-open "$torrentname/$filename" & ;;
		"mark") transmission-remote -t$torrentid -g$fileid ;;
		"unmark") transmission-remote -t$torrentid -G$fileid ;;
		"priority")
			priority=$(printf "high priority\nnormal priority\nlow priority" | dmenu -i -p "$filename")
			case $priority in
				"high priority") transmission-remote -t$torrentid -ph $fileid ;;
				"normal priority") transmission-remote -t$torrentid -pn $fileid ;;
				"low priority") transmission-remote -t$torrentid -pl $fileid ;;
			esac
			;;
	esac
}

managetorrents () {
	torrentline=$(listtorrents) || return
	torrentid=$(echo "$torrentline" | awk '{print $1}' | sed 's/*//g')
	torrentname="$(echo "$torrentline" | sed 's/.*  //g')"
	
	if [ -f "$torrentname" ];then
		action=$(printf "open\nset bandwidth\nset location\nstart torrent\nstop torrent\nverify torrent\ncopy magnet\nREMOVE TORRENT\nREMOVE TORRENT AND DELETE ALL FILES\n" | dmenu -i -l 10 -p "$torrentname:")
	else
		filecount=$(transmission-remote -t$torrentid -f | grep -c '^[[:space:]]*[0-9]')
		action=$(printf "manage ($filecount files)\nset bandwidth\nset location\nnsequence files\nmark all\nunmark all\nstart torrent\nstop torrent\nverify torrent\ncopy magnet\nREMOVE TORRENT\nREMOVE TORRENT AND DELETE ALL FILES\n" | dmenu -i -l 10 -p "$torrentname:")
	fi
	case "$action" in
		*"location") setlocation ;;
		"manage"*) managefiles ;;
		"start"*) transmission-remote -t$torrentid -s ;;
		"stop"*) transmission-remote -t$torrentid -S ;;
		*"magnet") transmission-remote -t$torrentid -i | sed -n 's/^.*Magnet: //p' | xsel --input ;;
		"mark"*) transmission-remote -t$torrentid -g all ;;
		"unmark"*) transmission-remote -t$torrentid -G all ;;
		"verify"*) transmission-remote -t$torrentid -v ;;
		"sequence"*) $TERMINAL sequence ;;
		*"bandwidth")
			bandwidth=$(printf "high bandwidth\nnormal bandwidth\nlow bandwidth" | dmenu -i -p "$torrentname")
			case "$bandwidth" in
				"high"*) transmission-remote -t$torrentid -Bh ;;
				"normal"*) transmission-remote -t$torrentid -Bn ;;
				"low"*) transmission-remote -t$torrentid -Bl ;;
			esac
			;;
		"REMOVE TORRENT")
			remove=$(printf "no\nYES" | dmenu -i -p "remove and keep data from $torrentname")
			[ "$remove" == "YES" ] && transmission-remote -t$torrentid -r
			;;
		"REMOVE TORRENT AND DELETE ALL FILES")
			delete=$(printf "no\nYES" | dmenu -i -p "remove and delete data from $torrentname")
			[ "$delete" == "YES" ] && transmission-remote -t$torrentid -rad
			;;
	esac
	managetorrents
}

mainmenu () {
	torrentcount="$(transmission-remote -l | grep -c '^[[:space:]]*[0-9]')"
	downspeed="$(transmission-remote -si | awk '/Download sp/ {print $4}')"
	upspeed="$(transmission-remote -si | awk '/Upload sp/ {print $4}')"
	action=$(printf "manage ($torrentcount torrents)\nweb interface\nspeed limits (down: $downspeed | up: $upspeed)\nstart all torrents\nstop all torrents\ndelete all torrents\nrestart daemon\nkill daemon" | dmenu -l 10 -p "transmission:")
	
	case "$action" in 
		"manage"*) managetorrents ;;
		"speed"*) speedlimits ;;
		"web"*) $BROWSER --new-tab "http://localhost:9091/transmission/web/" ;;
		"start"*) transmission-remote -t all -s ;;
		"stop"*) transmission-remote -t all -S ;;
		"delete"*) 
			deleteall=$(printf "no\nYES" | dmenu -i -p "Delete All Torrents")
			[ "$deleteall" == "YES" ] && transmission-remote --exit
			;;
		"restart"*) 
			restart=$(printf "no\nYES" | dmenu -i -p "initiate a restart")
			if [ "$restart" == "YES" ]; then
				transmission-remote --exit
				startdaemon
				sleep 3
				exec $(basename "$0")
			fi
			;;
		"kill"*) 
			shutdown=$(printf "no\nYES" | dmenu -i -p "initiate a shutdown")
			[ "$shutdown" == "YES" ] && transmission-remote --exit
			;;
		*) exit ;;
	esac
	mainmenu
}

if ! command -v transmission-remote &> /dev/null; then
	echo "Error: transmission-remote is not installed."
	exit 1
elif ! transmission-remote -l &> /dev/null ;then 
	action=$(printf "run\nexit" | dmenu -i -p "trasmission-daemon is not running:")
	[ "$action" == "run" ] && startdaemon || exit
	while ! transmission-remote -l &> /dev/null ;do
		sleep 0.3
	done
fi

if [ "$1" == "magnet:?"* ]; then
	if [ $startpaused -eq 1 ]; then
		transmission-remote --start-paused && /usr/bin/transmission-remote --add "$1"
	else
		transmission-remote --no-start-paused && /usr/bin/transmission-remote --add "$1"
		#mon -s "Torrent added: $(echo "$1" | sed 's/.*&dn=//g;s/&tr=.*//g;s/%../ /g;s/x264.*/x264/g' | cut -c -55)"
	fi
else
	mainmenu
fi
